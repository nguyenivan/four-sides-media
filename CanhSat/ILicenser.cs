﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KinectMouseControl
{
    [ComImport]
    [Guid ("F62986FA-756D-4AAF-BC67-D3B071507217")]
    public interface IKinectLicenser  
    {
        string GetLicense(IList<string> kinectUniqueIdList);
    }
}
