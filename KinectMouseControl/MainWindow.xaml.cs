﻿using System;
using System.Collections.Generic;
using System.Windows;
using Microsoft.Kinect;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.CompilerServices;
using KinectEx.Smoothing;
using KinectEx;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Text;

namespace KinectMouseControl
{
    /// <summary>
    /// MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window, INotifyPropertyChanged
    {
        const int INPUT_MOUSE = 0;
        const int INPUT_KEYBOARD = 1;
        const int INPUT_HARDWARE = 2;
        const uint MOUSEEVENTF_MOVE = 0x0001;
        const uint MOUSEEVENTF_ABSOLUTE = 0x8000;
        const uint MOUSEEVENTF_LEFTDOWN = 0x02;
        const uint MOUSEEVENTF_LEFTUP = 0x04;
        const uint KEYEVENTF_SCANCODE = 0x0008;
        const uint KEYEVENTF_KEYUP = 0x0002;
        const uint MOUSEEVENTF_RIGHTDOWN = 0x0008;
        const uint MOUSEEVENTF_RIGHTUP = 0x0010;

        enum ClickMode
        {
            Clench,
            Hold
        }

        struct INPUT
        {
            public int type;
            public InputUnion u;
        }

        [StructLayout(LayoutKind.Explicit)]
        struct InputUnion
        {
            [FieldOffset(0)]
            public MOUSEINPUT mi;
            [FieldOffset(0)]
            public KEYBOARDINPUT ki;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct MOUSEINPUT
        {
            public int dx;
            public int dy;
            public uint mouseData;
            public uint dwFlags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct KEYBOARDINPUT
        {
            public ushort wVk;
            public ushort wScan;
            public uint dwFlags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetCursorPos(ref Win32Point pt);

        [StructLayout(LayoutKind.Sequential)]
        internal struct Win32Point
        {
            public Int32 X;
            public Int32 Y;
        };

        public static Point GetMousePosition()
        {
            Win32Point w32Mouse = new Win32Point();
            GetCursorPos(ref w32Mouse);
            return new Point(w32Mouse.X, w32Mouse.Y);
        }

        struct HandInfo
        {
            public HandState HandState;
            public CameraSpacePoint Point;
            public IJoint HandJoint;
        }

        [DllImport("user32.dll")]
        static extern IntPtr GetMessageExtraInfo();

        [DllImport("user32.dll", SetLastError = true)]
        static extern uint SendInput(uint nInputs, INPUT[] pInputs, int cbSize);

        [DllImport("user32.dll")]
        static extern byte VkKeyScan(char ch);

        [DllImport("user32.dll")]
        public static extern uint MapVirtualKey(uint uCode, uint uMapType);

        /// <summary>
        /// Press the key corresponding to the specified scancode down.
        /// </summary>
        /// <param name="scanCode">The scancode of the key to press down.</param>
        public static void SendKeyDown(ushort scanCode)
        {
            INPUT[] inputs = new INPUT[1];
            inputs[0] = new INPUT
            {
                type = INPUT_KEYBOARD,
                u = new InputUnion
                {
                    ki = new KEYBOARDINPUT
                    {
                        wVk = 0,
                        wScan = scanCode,
                        time = 0,
                        dwExtraInfo = (System.IntPtr)0,
                        dwFlags = KEYEVENTF_SCANCODE
                    }
                }
            };

            SendInput(1, inputs, System.Runtime.InteropServices.Marshal.SizeOf(inputs[0]));
        }

        /// <summary>
        /// Press the key corresponding to the specified scancode up.
        /// </summary>
        /// <param name="scanCode">The scancode of the key to press up.</param>
        public static void SendKeyUp(ushort scanCode)
        {
            INPUT[] inputs = new INPUT[1];
            inputs[0] = new INPUT
            {
                type = INPUT_KEYBOARD,
                u =
                {
                    ki =
                    {
                        wScan = scanCode,
                        dwFlags = KEYEVENTF_KEYUP | KEYEVENTF_SCANCODE
                    }
                }
            };
            SendInput(1, inputs, System.Runtime.InteropServices.Marshal.SizeOf(inputs[0]));
        }

        /// <summary>
        /// Maps characters into their scancodes and then either presses the corresponding
        /// key up or down.
        /// </summary>
        /// <param name="ch">The character to press up or down.</param>
        /// <param name="press">To press keys down make press 'true', and 'false' to press keys up.</param>
        private static void PressKey(char ch, bool press)
        {
            byte vk = VkKeyScan(ch);
            ushort scanCode = (ushort)MapVirtualKey(vk, 0);

            if (press)
                SendKeyDown(scanCode);
            else
                SendKeyUp(scanCode);
        }

        private static int POINT_BUFFER_SIZE = Properties.Settings.Default.BufferSize;
        private int _pointBufferSize;


        public int PointBufferSize
        {
            get { return _pointBufferSize; }
            set
            {
                _pointBufferSize = value;
                this.pointBuffer = new FixedSizeQueue<CameraSpacePoint>(_pointBufferSize);
            }
        }

        private FixedSizeQueue<CameraSpacePoint> pointBuffer = null;

        private FixedSizeQueue<HandInfo> gestureBuffer = null;

        private static readonly float INFINITY = 10000;
        private CameraSpacePoint lastHandRight = new CameraSpacePoint() { X = INFINITY };
        private static int GESTURE_BUFFER_SIZE = 60;
        private static float CLENCH_PERCENTILE = 0.1f;
        private static float LONG_CLENCH_PERCENTILE = 0.7f;
        private static float TRACKED_PERCENTILE = 0.9f;
        private float speedScale = Properties.Settings.Default.SpeedScale; // one centimet = 1 pixel;
        private static string BUTTON_TEXT_CLOSE_PROMPT = "Click Here To Close";
        private static string BUTTON_TEXT_TRACK_PROMPT = "Raise Right Hand To Start";
        private static ulong UNTRACKED_ID = 0;
        private ulong _trackingId = UNTRACKED_ID;


        private KeyboardMouseGestures _gesture;

        /// <summary>
        /// In seconds
        /// </summary>
        private const int CHECK_LICENSE_INTERVAL = 10;

        public MainWindow()
        {
            this.kinectSensor = KinectSensor.GetDefault();
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();
            this.depthFrameReader = this.kinectSensor.DepthFrameSource.OpenReader();
            this.kinectSensor.Open();
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;
            this.depthFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
            this.displayWidth = depthFrameDescription.Width;
            this.displayHeight = depthFrameDescription.Height;
            this.realTimeImage = new WriteableBitmap(this.displayWidth, this.displayHeight, 96.0, 96.0, PixelFormats.Gray8, null);
            this.bodyArray = new Body[this.kinectSensor.BodyFrameSource.BodyCount];
            this.drawingGroup = new DrawingGroup();
            this.imageSource = new DrawingImage(this.drawingGroup);
            // allocate space to put the pixels being received and converted
            this.depthPixels = new byte[this.depthFrameDescription.Width * this.depthFrameDescription.Height];
            this.DataContext = this;
            this.PointBufferSize = POINT_BUFFER_SIZE;
            this.customBodies = new List<CustomBody>();
            this.kalmanBodies = new SmoothedBodyList<KalmanSmoother>();
            this.exponentialBodies = new SmoothedBodyList<ExponentialSmoother>();
            this.gestureBuffer = new FixedSizeQueue<HandInfo>(GESTURE_BUFFER_SIZE);

            _gesture = new KeyboardMouseGestures(this.kinectSensor);

            _gesture.BeginGrab += _gesture_BeginGrab;
            _gesture.EndGrab += _gesture_EndGrab;
            _gesture.Clenched += _gesture_Clenched;
            _gesture.EndDrag += _gesture_EndDrag;
            _gesture.BeginDrag += _gesture_BeginDrag;
            _gesture.LeftClenched += _gesture_LeftClenched;
            _gesture.OpenHandInward += _gesture_OpenHandInward;
            _gesture.OpenHandOutward += _gesture_OpenHandOutward;

            // Checking license
            _licenseTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(CHECK_LICENSE_INTERVAL) };
            _licenseTimer.Start();
            _licenseTimer.Tick += _licenseTimer_Tick;

            InitializeComponent();

            this.ButtonMain.Content = BUTTON_TEXT_TRACK_PROMPT;

        }

        async void _licenseTimer_Tick(object sender, EventArgs e)
        {
            int limitOfTrial = 3;
            int delayMilliseconds = 1000;
            for (int i = 0; i < limitOfTrial; i++)
            {
                if (kinectSensor.IsOpen && !string.IsNullOrWhiteSpace(kinectSensor.UniqueKinectId))
                {
                    try
                    {
                        KinectLicenser.ValidateLicense(kinectSensor.UniqueKinectId, KinectLicenser.FileName);
                        break;
                    }
                    catch (KinectLicenseException ex)
                    {
                        kinectSensor.Close();
                        _licenseTimer.Stop();
                        KinectLicenseError box = new KinectLicenseError(ex.Message, kinectSensor.UniqueKinectId);
                        var boxRet = box.ShowDialog();
                    }
                }
                await Task.Delay(delayMilliseconds);
            }
        }

        void _gesture_EndGrab(object sender, EventArgs e)
        {
            pointBuffer.Clear();
            this.lastX = float.PositiveInfinity;
            this.lastY = float.PositiveInfinity;
            _allowMove = true;
        }

        /// <summary>
        /// Send numpad +
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _gesture_OpenHandOutward(object sender, EventArgs e)
        {
            SendKeyDown((ushort)MapVirtualKey(0x6B, 0));
            pointBuffer.Clear();

        }

        /// <summary>
        /// Send numpad -
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _gesture_OpenHandInward(object sender, EventArgs e)
        {
            //PressKey('-', true);
            SendKeyDown((ushort)MapVirtualKey(0x6D, 0));
            pointBuffer.Clear();
        }

        private void _gesture_LeftClenched(object sender, EventArgs e)
        {
            // Send mouse right up event
            var inputs = new List<INPUT>();
            inputs.Add(new INPUT
            {
                type = INPUT_MOUSE,
                u = new InputUnion
                {
                    mi = new MOUSEINPUT
                    {
                        dwFlags = MOUSEEVENTF_RIGHTUP | MOUSEEVENTF_RIGHTDOWN
                    }
                }
            });

            var inputArray = inputs.ToArray();
            SendInput((uint)inputArray.Length, inputArray, Marshal.SizeOf(typeof(INPUT)));
        }


        #region Grab Event Handlers
        void _gesture_EndDrag(object sender, EventArgs e)
        {
            var inputs = new List<INPUT>();
            inputs.Add(new INPUT
            {
                type = INPUT_MOUSE,
                u = new InputUnion
                {
                    mi = new MOUSEINPUT
                    {
                        dwFlags = MOUSEEVENTF_LEFTUP
                    }
                }
            });

            var inputArray = inputs.ToArray();
            SendInput((uint)inputArray.Length, inputArray, Marshal.SizeOf(typeof(INPUT)));
            this.pointBuffer.Clear();
            this.lastX = float.PositiveInfinity;
            this.lastY = float.PositiveInfinity;
            _allowMove = true;
        }

        void _gesture_Clenched(object sender, EventArgs e)
        {
            // Send mouse up event
            var inputs = new List<INPUT>();
            inputs.Add(new INPUT
            {
                type = INPUT_MOUSE,
                u = new InputUnion
                {
                    mi = new MOUSEINPUT
                    {
                        dwFlags = MOUSEEVENTF_LEFTUP | MOUSEEVENTF_LEFTDOWN
                    }
                }
            });

            var inputArray = inputs.ToArray();
            SendInput((uint)inputArray.Length, inputArray, Marshal.SizeOf(typeof(INPUT)));
            pointBuffer.Clear();
            this.lastX = float.PositiveInfinity;
            this.lastY = float.PositiveInfinity;
            _allowMove = true;
        }

        void _gesture_BeginDrag(object sender, EventArgs e)
        {
            var inputs = new List<INPUT>();
            inputs.Add(new INPUT
            {
                type = INPUT_MOUSE,
                u = new InputUnion
                {
                    mi = new MOUSEINPUT
                    {
                        dwFlags = MOUSEEVENTF_LEFTDOWN
                    }
                }
            });

            var inputArray = inputs.ToArray();
            SendInput((uint)inputArray.Length, inputArray, Marshal.SizeOf(typeof(INPUT)));

            pointBuffer.Clear();
            this.lastX = float.PositiveInfinity;
            this.lastY = float.PositiveInfinity;
            _allowMove = true;
        }

        void _gesture_BeginGrab(object sender, EventArgs e)
        {
            _allowMove = false;
        }

        #endregion
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                this.bodyFrameReader.FrameArrived += bodyFrameReader_FrameArrived;
            }
            if (this.depthFrameReader != null)
            {
                this.depthFrameReader.FrameArrived += depthFrameReader_FrameArrived;
            }
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                // BodyFrameReader is IDisposable
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
            if (_gesture != null)
            {
                _gesture.Dispose();
                _gesture = null;
            }
        }

        private void depthFrameReader_FrameArrived(object sender, DepthFrameArrivedEventArgs e)
        {
            bool depthFrameProcessed = false;

            using (DepthFrame depthFrame = e.FrameReference.AcquireFrame())
            {
                if (depthFrame != null)
                {
                    // the fastest way to process the body index data is to directly access 
                    // the underlying buffer
                    using (Microsoft.Kinect.KinectBuffer depthBuffer = depthFrame.LockImageBuffer())
                    {
                        // verify data and write the color data to the display bitmap
                        if (((this.depthFrameDescription.Width * this.depthFrameDescription.Height) == (depthBuffer.Size / this.depthFrameDescription.BytesPerPixel)) &&
                            (this.depthFrameDescription.Width == this.realTimeImage.PixelWidth) && (this.depthFrameDescription.Height == this.realTimeImage.PixelHeight))
                        {
                            // Note: In order to see the full range of depth (including the less reliable far field depth)
                            // we are setting maxDepth to the extreme potential depth threshold
                            ushort maxDepth = ushort.MaxValue;

                            // If you wish to filter by reliable depth distance, uncomment the following line:
                            //// maxDepth = depthFrame.DepthMaxReliableDistance

                            this.ProcessDepthFrameData(depthBuffer.UnderlyingBuffer, depthBuffer.Size, depthFrame.DepthMinReliableDistance, maxDepth);
                            depthFrameProcessed = true;
                        }
                    }
                }
            }

            if (depthFrameProcessed)
            {
                this.RenderDepthPixels();
            }
        }

        /// <summary>
        /// Directly accesses the underlying image buffer of the DepthFrame to 
        /// create a displayable bitmap.
        /// This function requires the /unsafe compiler option as we make use of direct
        /// access to the native memory pointed to by the depthFrameData pointer.
        /// </summary>
        /// <param name="depthFrameData">Pointer to the DepthFrame image data</param>
        /// <param name="depthFrameDataSize">Size of the DepthFrame image data</param>
        /// <param name="minDepth">The minimum reliable depth value for the frame</param>
        /// <param name="maxDepth">The maximum reliable depth value for the frame</param>
        private unsafe void ProcessDepthFrameData(IntPtr depthFrameData, uint depthFrameDataSize, ushort minDepth, ushort maxDepth)
        {
            // depth frame data is a 16 bit value
            ushort* frameData = (ushort*)depthFrameData;

            // convert depth to a visual representation
            for (int i = 0; i < (int)(depthFrameDataSize / this.depthFrameDescription.BytesPerPixel); ++i)
            {
                // Get the depth for this pixel
                ushort depth = frameData[i];

                // To convert to a byte, we're mapping the depth value to the byte range.
                // Values outside the reliable depth range are mapped to 0 (black).
                this.depthPixels[i] = (byte)(depth >= minDepth && depth <= maxDepth ? (depth / MapDepthToByte) : 0);
            }
        }

        /// <summary>
        /// Renders color pixels into the writeableBitmap.
        /// </summary>
        private void RenderDepthPixels()
        {
            this.realTimeImage.WritePixels(
                new Int32Rect(0, 0, this.realTimeImage.PixelWidth, this.realTimeImage.PixelHeight),
                this.depthPixels,
                this.realTimeImage.PixelWidth,
                0);
        }

        void bodyFrameReader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            bool dataReceived = false;
            IEnumerable<IBody> bodies = null;
            using (DrawingContext dc = this.drawingGroup.Open())
            {
                // Draw a transparent background to set the render size

                dc.DrawRectangle(Brushes.Transparent, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                // draw the kinect bitmap if it's there
                if (null != RealTimeImage)
                {
                    // determine the coordinates for displaying the image
                    Double w = realTimeImage.Width * this.displayHeight / realTimeImage.Height;
                    Double diffWidth = Math.Abs(this.displayWidth - w);
                    Double x = -(diffWidth / 2);
                    Double ww = w + x;
                    dc.DrawImage(realTimeImage, new Rect(x, 0.00, w, this.displayHeight));
                }

                using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
                {
                    if (bodyFrame != null)
                    {
                        bodyFrame.GetAndRefreshBodyData(this.kalmanBodies);
                        bodies = this.kalmanBodies;
                        foreach (IBody body in bodies)
                        {
                            if (!body.IsTracked)
                            {
                                continue;
                            }

                            if (body.TrackingId == _trackingId ||
                                (_trackingId == UNTRACKED_ID && CheckRightHandRaised(body)))
                            {
                                // convert the joint points to depth (display) space
                                Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();
                                foreach (JointType jointType in body.Joints.Keys)
                                {
                                    DepthSpacePoint colorSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(body.Joints[jointType].Position);
                                    jointPoints[jointType] = new Point(colorSpacePoint.X, colorSpacePoint.Y);
                                }
                                this.DrawBody(body.Joints, jointPoints, dc);
                                this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                                this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);

                                if (_trackingId == UNTRACKED_ID && CheckRightHandRaised(body)) // First time we have tracking
                                {
                                    _trackingId = body.TrackingId;
                                    _gesture.TrackingId = _trackingId;
                                    pointBuffer.Clear();
                                    SetCursorCenter();
                                    SetButtonText(BUTTON_TEXT_CLOSE_PROMPT);
                                    lastX = float.PositiveInfinity;
                                    lastY = float.PositiveInfinity;
                                }
                                else if (_allowMove && body.Joints[JointType.HandRight].TrackingState == TrackingState.Tracked)
                                {
                                    var point = body.Joints[JointType.HandRight].Position;
                                    var h = new HandInfo
                                    {
                                        HandState = body.HandRightState,
                                        Point = point,
                                        HandJoint = body.Joints[JointType.HandRight]
                                    };
                                    pointBuffer.Enqueue(point);

                                    CameraSpacePoint? filtered = SimpleAverageFilter();
                                    if (filtered != null)
                                    {
                                        float x = filtered.Value.X;
                                        float y = filtered.Value.Y;
                                        if (float.IsInfinity(lastX))
                                        {
                                            this.lastX = x;
                                        }
                                        if (float.IsInfinity(lastY))
                                        {
                                            this.lastY = y;
                                        }

                                        xValue.Text = "X:" + point.X.ToString();
                                        yValue.Text = "Y:" + point.Y.ToString();
                                        var inputs = new List<INPUT>();

                                        var deltaX = x - this.lastX;
                                        var deltaY = -(y - this.lastY);

                                        inputs.Add(new INPUT
                                        {
                                            type = INPUT_MOUSE,
                                            u = new InputUnion
                                            {
                                                mi = new MOUSEINPUT
                                                {
                                                    dwFlags = MOUSEEVENTF_MOVE,
                                                    dx = (int)(65535 * deltaX * speedScale),
                                                    dy = (int)(65535 * deltaY * speedScale),
                                                }
                                            }
                                        });

                                        var inputArray = inputs.ToArray();
                                        SendInput((uint)inputArray.Length, inputArray, Marshal.SizeOf(typeof(INPUT)));
                                        this.lastX = x;
                                        this.lastY = y;
                                    }

                                }
                                else if (body.Joints[JointType.HandRight].TrackingState != TrackingState.Tracked) //Reset tracking
                                {
                                    _trackingId = UNTRACKED_ID;
                                    _gesture.TrackingId = UNTRACKED_ID;
                                    pointBuffer.Clear();
                                    SetCursorCenter();
                                    SetButtonText(BUTTON_TEXT_TRACK_PROMPT);
                                    lastX = float.PositiveInfinity;
                                    lastY = float.PositiveInfinity;
                                }


                                dataReceived = true;
                                break; // only track one skeleton
                            }
                        }
                        if (!dataReceived && _trackingId != UNTRACKED_ID) // first time we loose track
                        {
                            _trackingId = UNTRACKED_ID;
                            SetButtonText(BUTTON_TEXT_TRACK_PROMPT);
                        }
                        // prevent drawing outside of our render area
                        this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                    }
                }
            }
            //if (dataReceived)
            //{
            //    var body = bodies.Where(b => b.IsTracked && (this.trackedId == null || b.TrackingId == this.trackedId.Value)).FirstOrDefault();
            //    if (body == null)
            //    {
            //        this.trackedId = null;
            //    }
            //    else
            //    {
            //        if (trackedId == null)
            //        {
            //            trackedId = body.TrackingId;
            //            _gesture.TrackingId = body.TrackingId;
            //        }
            //    }
            //}
        }

        private bool CheckHand()
        {
            if (gestureBuffer.Where(x => x.HandJoint.TrackingState == TrackingState.Tracked).Count() < GESTURE_BUFFER_SIZE * TRACKED_PERCENTILE) return false;
            return true;
        }

        private bool CheckClench()
        {
            if (gestureBuffer.Where(x => x.HandState == HandState.Closed).Count() < GESTURE_BUFFER_SIZE * CLENCH_PERCENTILE) return false;
            return true;//Not qualified for hold
        }

        private bool CheckLongClench()
        {
            if (gestureBuffer.Where(x => x.HandState == HandState.Closed).Count() < GESTURE_BUFFER_SIZE * LONG_CLENCH_PERCENTILE) return false;
            return true;//Not qualified for hold
        }


        private Point Accelerate(Point delta)
        {
            //(1/(0.5+4))*  x*(Abs(x)+4)
            float linearity = 0.3F;   // higher values mean more linear curve, less acceleration
            float intersection = 0.05F; // point where accelerated curve intersects with linear curve

            //Formula:(with Mathf.Abs we can keep the minus for negative values after the multiplication)
            double curMovX = (1F / (intersection + linearity)) * delta.X * (Math.Abs(delta.X) * linearity);
            double curMovY = (1F / (intersection + linearity)) * delta.Y * (Math.Abs(delta.Y) * linearity);

            //Clamp if needed
            curMovX = Math.Min(Math.Max(curMovX, -linearity), linearity);
            curMovY = Math.Min(Math.Max(curMovY, -linearity), linearity);
            return new Point { X = curMovX, Y = curMovY };
        }

        private CameraSpacePoint? SimpleAverageFilter()
        {
            CameraSpacePoint ret = new CameraSpacePoint();
            if (pointBuffer.TryPeek(out ret))
            {
                return ret;
            }
            else
            {
                return null;
            }
        }


        #region Private members
        /// <summary>
        /// Radius of drawn hand circles
        /// </summary>
        private const double HandSize = 30;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// </summary>
        private readonly Brush handClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// </summary>
        private readonly Brush handOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// </summary>
        private readonly Brush handLassoBrush = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));

        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private readonly int bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Reader for body
        /// frames
        /// </summary>
        private DepthFrameReader depthFrameReader = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodyArray = null;

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        private WriteableBitmap realTimeImage = null;
        /// <summary>
        /// Intermediate storage for frame data converted to color
        /// </summary>
        private byte[] depthPixels = null;
        private FrameDescription depthFrameDescription;
        /// <summary>
        /// Map depth range to byte range
        /// </summary>
        private const int MapDepthToByte = 8000 / 256;
        #endregion

        #region Canvas Drawing Methods
        /// <summary>
        /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
        /// </summary>
        /// <param name="handState">state of the hand</param>
        /// <param name="handPosition">position of the hand</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawHand(HandState handState, Point handPosition, DrawingContext drawingContext)
        {
            switch (handState)
            {
                case HandState.Closed:
                    drawingContext.DrawEllipse(this.handClosedBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Open:
                    drawingContext.DrawEllipse(this.handOpenBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Lasso:
                    drawingContext.DrawEllipse(this.handLassoBrush, null, handPosition, HandSize, HandSize);


                    break;
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBone(IReadOnlyDictionary<JointType, IJoint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext)
        {
            var joint0 = joints[jointType0];
            var joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == TrackingState.Inferred &&
                joint1.TrackingState == TrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
        }

        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBody(IReadOnlyDictionary<JointType, IJoint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext)
        {
            // Draw the bones

            // Torso
            this.DrawBone(joints, jointPoints, JointType.Head, JointType.Neck, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.Neck, JointType.SpineShoulder, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.SpineMid, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineMid, JointType.SpineBase, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipLeft, drawingContext);

            // Right Arm    
            this.DrawBone(joints, jointPoints, JointType.ShoulderRight, JointType.ElbowRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowRight, JointType.WristRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.HandRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandRight, JointType.HandTipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.ThumbRight, drawingContext);

            // Left Arm
            this.DrawBone(joints, jointPoints, JointType.ShoulderLeft, JointType.ElbowLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowLeft, JointType.WristLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.HandLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandLeft, JointType.HandTipLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.ThumbLeft, drawingContext);

            // Right Leg
            this.DrawBone(joints, jointPoints, JointType.HipRight, JointType.KneeRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeRight, JointType.AnkleRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleRight, JointType.FootRight, drawingContext);

            // Left Leg
            this.DrawBone(joints, jointPoints, JointType.HipLeft, JointType.KneeLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeLeft, JointType.AnkleLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleLeft, JointType.FootLeft, drawingContext);

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }
        #endregion


        public WriteableBitmap RealTimeImage
        {
            get { return realTimeImage; }
            set
            {
                realTimeImage = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private float lastX = float.PositiveInfinity;
        private float lastY = float.PositiveInfinity;
        private SmoothedBodyList<KalmanSmoother> kalmanBodies;
        private SmoothedBodyList<ExponentialSmoother> exponentialBodies;
        private List<CustomBody> customBodies;
        private ulong? trackedId;
        private bool _allowMove = true;
        private DispatcherTimer _licenseTimer;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.imageSource;
            }
        }

        private void MouseSpeedSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.speedScale = (float)e.NewValue;
        }

        private void TextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
        }

        private void TextBox_TextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Properties.Settings.Default.SpeedScale = this.speedScale;
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            Window window = (Window)sender;
            window.Topmost = true;
            //this.WindowState = WindowState.Minimized;
        }

        private void Big_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right - this.Width;
            this.Top = desktopWorkingArea.Bottom - this.Height;
        }

        private void Min_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        private bool CheckRightHandRaised(IBody body)
        {
            //return body.IsTracked;
            if (!body.IsTracked ||
                body.Joints[JointType.HandRight].TrackingState == TrackingState.NotTracked ||
                body.Joints[JointType.Neck].TrackingState == TrackingState.NotTracked ||
                body.Joints[JointType.HandRight].Position.Y < body.Joints[JointType.Neck].Position.Y)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void SetButtonText(string text)
        {
            this.ButtonMain.Content = text;
        }

        private void SetCursorCenter()
        {
            //var centerX = SystemParameters.PrimaryScreenWidth / 2;
            //var centerY = SystemParameters.PrimaryScreenHeight / 2;

            var inputs = new List<INPUT>();

            inputs.Add(new INPUT
            {
                type = INPUT_MOUSE,
                u = new InputUnion
                {
                    mi = new MOUSEINPUT
                    {
                        dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE,
                        dx = (int)(65535.0 / 2 - 1),
                        dy = (int)(65535.0 / 2 - 1),
                    }
                }
            });

            var inputArray = inputs.ToArray();
            SendInput((uint)inputArray.Length, inputArray, Marshal.SizeOf(typeof(INPUT)));
            //System.Windows.Forms.Cursor.Position = new System.Drawing.Point(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width / 2,
            //                System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height / 2);
        }

    }
}
