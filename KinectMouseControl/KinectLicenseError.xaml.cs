﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KinectMouseControl
{
    /// <summary>
    /// Interaction logic for KinectLicenseError.xaml
    /// </summary>
    public partial class KinectLicenseError : Window
    {

        public KinectLicenseError(string errorMessage, string kinectUniqueId)
        {
            InitializeComponent();
            TextBlockErrorMessage.Text = errorMessage;
            TextBoxKinectUniqueId.Text = kinectUniqueId;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
