﻿using Microsoft.Kinect;
using Microsoft.Kinect.VisualGestureBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectMouseControl
{
    public class GrabGesture : IDisposable
    {
        /// <summary> Path to the gesture database that was trained with VGB </summary>
        private readonly string gestureDatabase = @"Gestures\Gestures.gbd";

        /// <summary> Name of the discrete gesture in the database that we want to track </summary>
        private readonly string seatedGestureName = "Grab_Right";

        /// <summary> Gesture frame source which should be tied to a body tracking ID </summary>
        private VisualGestureBuilderFrameSource vgbFrameSource = null;

        /// <summary> Gesture frame reader which will handle gesture events coming from the sensor </summary>
        private VisualGestureBuilderFrameReader vgbFrameReader = null;


        float _confidenceThreshold = 0.02f;

        DateTime _startGrab = DateTime.Now;

        /// <summary>
        /// Time window for clench event to happen
        /// </summary>
        const int CLENCH_WINDOW = 700;

        TimeSpan _clenchWindow = new TimeSpan(0, 0, 0, 0, CLENCH_WINDOW);

        /// <summary>
        /// How many frame kept in the buffer
        /// </summary>
        const int BUFFER_LENGTH = 5;

        /// <summary>
        /// The ratio of good frames in buffer needed to maintain the state of the gesture
        /// </summary>
        const float FRAME_ACCURACY = 0.8f;

        // Need to raise to prevent the mouse from moving
        public event EventHandler BeginGrab = delegate { };

        // When the Grab_Right gesture happens within time window
        public event EventHandler Clenched = delegate { };

        // If still in the state and ouside of time window
        public event EventHandler BeginDrag = delegate { };

        // This event need to be handled to resume regular mouse movement
        public event EventHandler EndDrag = delegate { };

        FixedSizedQueue<bool> _grabBuffer = new FixedSizedQueue<bool>(BUFFER_LENGTH);
        private bool _grabbing = false;

        public GrabGesture(KinectSensor kinectSensor)
        {
            if (kinectSensor == null)
            {
                throw new ArgumentNullException("kinectSensor");
            }


            // create the vgb source. The associated body tracking ID will be set when a valid body frame arrives from the sensor.
            this.vgbFrameSource = new VisualGestureBuilderFrameSource(kinectSensor, 0);

            // open the reader for the vgb frames
            this.vgbFrameReader = this.vgbFrameSource.OpenReader();
            if (this.vgbFrameReader != null)
            {
                this.vgbFrameReader.IsPaused = false;
                this.vgbFrameReader.FrameArrived += this.Reader_GestureFrameArrived;
            }

            // load the 'Seated' gesture from the gesture database
            using (VisualGestureBuilderDatabase database = new VisualGestureBuilderDatabase(this.gestureDatabase))
            {
                // we could load all available gestures in the database with a call to vgbFrameSource.AddGestures(database.AvailableGestures), 
                // but for this program, we only want to track one discrete gesture from the database, so we'll load it by name
                foreach (Gesture gesture in database.AvailableGestures)
                {
                    if (gesture.Name.Equals(this.seatedGestureName))
                    {
                        this.vgbFrameSource.AddGesture(gesture);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the body tracking ID associated with the current detector
        /// The tracking ID can change whenever a body comes in/out of scope
        /// </summary>
        public ulong TrackingId
        {
            get
            {
                return this.vgbFrameSource.TrackingId;
            }

            set
            {
                if (this.vgbFrameSource.TrackingId != value)
                {
                    this.vgbFrameSource.TrackingId = value;
                }
            }
        }

        /// <summary>
        /// Handles gesture detection results arriving from the sensor for the associated body tracking Id
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_GestureFrameArrived(object sender, VisualGestureBuilderFrameArrivedEventArgs e)
        {
            VisualGestureBuilderFrameReference frameReference = e.FrameReference;
            using (VisualGestureBuilderFrame frame = frameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    // get the discrete gesture results which arrived with the latest frame
                    IReadOnlyDictionary<Gesture, DiscreteGestureResult> discreteResults = frame.DiscreteGestureResults;

                    if (discreteResults != null)
                    {
                        // we only have one gesture in this source object, but you can get multiple gestures
                        foreach (Gesture gesture in this.vgbFrameSource.Gestures)
                        {
                            if (gesture.Name.Equals(this.seatedGestureName) && gesture.GestureType == GestureType.Discrete)
                            {
                                DiscreteGestureResult result = null;
                                discreteResults.TryGetValue(gesture, out result);

                                if (result != null)
                                {
                                    ProcessGesture(result);

                                }
                            }
                        }
                    }
                }
            }
        }

        private void ProcessGesture(DiscreteGestureResult result)
        {
            var firstFrame = result.FirstFrameDetected && (_grabBuffer.IsEmpty);
            if (firstFrame)
            {
                _startGrab = DateTime.Now;
                BeginGrab(this, new EventArgs());
                _grabbing = true;
            }

            if (_grabbing)
            {
                bool positive = result.Detected && (result.Confidence > _confidenceThreshold);
                _grabBuffer.Enqueue(positive);
            }

            if (firstFrame)
            {
                return;
            }

            TimeSpan duration = DateTime.Now - _startGrab;
            var positiveCount = _grabBuffer.Where(x => x).Count();
            if (positiveCount > _grabBuffer.Count * FRAME_ACCURACY)
            {
                if (duration >= _clenchWindow)
                {
                    BeginDrag(this, new EventArgs());
                }
            }
            else //End it and clear the queue
            {
                if (_grabbing)
                {
                    _grabbing = false;
                    _grabBuffer.Clear();
                    if (duration < _clenchWindow)
                    {
                        Clenched(this, new EventArgs());
                    }
                    else
                    {
                        EndDrag(this, new EventArgs());
                    }
                }
            }
        }

        /// <summary>
        /// Disposes all unmanaged resources for the class
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the VisualGestureBuilderFrameSource and VisualGestureBuilderFrameReader objects
        /// </summary>
        /// <param name="disposing">True if Dispose was called directly, false if the GC handles the disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.vgbFrameReader != null)
                {
                    this.vgbFrameReader.FrameArrived -= this.Reader_GestureFrameArrived;
                    this.vgbFrameReader.Dispose();
                    this.vgbFrameReader = null;
                }

                if (this.vgbFrameSource != null)
                {
                    this.vgbFrameSource.Dispose();
                    this.vgbFrameSource = null;
                }
            }
        }
    }
}
