﻿using Microsoft.Kinect;
using Microsoft.Kinect.VisualGestureBuilder;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace KinectMouseControl
{
    public class KeyboardMouseGestures : IDisposable
    {


        /// <summary> Path to the gesture database that was trained with VGB </summary>
        private IReadOnlyList<string> _dbCollection = new List<string>()
        {
            @"Gestures\KinectKeyboardMouse.gbd"
        };


        /// <summary> Name of the discrete gesture in the database that we want to track </summary>
        private const string GRAB_RIGHT_GESTURE = "Grab_Right";
        private const string GRAB_LEFT_GESTURE = "Grab_Left";
        private const string OPEN_HAND_INWARD_GESTURE = "OpenHandInward";
        private const string OPEN_HAND_OUTWARD_GESTURE = "OpenHandOutward";

        private IReadOnlyDictionary<string, float> _gestureThreshold = new Dictionary<string, float>()
        {
            {GRAB_RIGHT_GESTURE, .2f},
            {GRAB_LEFT_GESTURE, .2f},
            {OPEN_HAND_INWARD_GESTURE, .2f},
            {OPEN_HAND_OUTWARD_GESTURE, .2f}
        };

        private const float DEFAULT_GESTURE_THRESHOLD = .5f;

        /// <summary> Gesture frame source which should be tied to a body tracking ID </summary>
        private VisualGestureBuilderFrameSource vgbFrameSource = null;

        /// <summary> Gesture frame reader which will handle gesture events coming from the sensor </summary>
        private VisualGestureBuilderFrameReader vgbFrameReader = null;


        float _confidenceThreshold = 0.1f;

        DateTime _startGrab = DateTime.Now;

        /// <summary>
        /// Time window for clench event to happen
        /// </summary>
        const int CLENCH_WINDOW = 700;

        TimeSpan _clenchWindow = new TimeSpan(0, 0, 0, 0, CLENCH_WINDOW);

        /// <summary>
        /// How many frame kept in the buffer
        /// </summary>
        const int BUFFER_LENGTH = 5;

        /// <summary>
        /// The ratio of good frames in buffer needed to maintain the state of the gesture
        /// </summary>
        const float FRAME_ACCURACY = 0.4f;
        private float GRAB_LEFT_FRAME_ACCURACY = 0.8f;


        // Need to raise to prevent the mouse from moving
        public event EventHandler BeginGrab = delegate { Debug.WriteLine("Begin Grab"); };

        public event EventHandler EndGrab = delegate { Debug.WriteLine("End Grab"); };

        // When the Grab_Right gesture happens within time window
        public event EventHandler Clenched = delegate {
            Debug.WriteLine("Clenched"); };

        // If still in the state and ouside of time window
        public event EventHandler BeginDrag = delegate {
            Debug.WriteLine("Begin Drag"); 
        };

        // This event need to be handled to resume regular mouse movement
        public event EventHandler EndDrag = delegate { Debug.WriteLine("End Drag"); };

        public event EventHandler LeftClenched = delegate { Debug.WriteLine("Left Clenched"); };

        public event EventHandler OpenHandInward = delegate { };

        public event EventHandler OpenHandOutward = delegate { };


        FixedSizeQueue<bool> _grabBuffer = new FixedSizeQueue<bool>(BUFFER_LENGTH);
        FixedSizeQueue<bool> _releaseBuffer = new FixedSizeQueue<bool>(BUFFER_LENGTH);

        IReadOnlyDictionary<string, FixedSizeQueue<bool>> _bufferDict = new Dictionary<string, FixedSizeQueue<bool>>() {
            {GRAB_RIGHT_GESTURE, new FixedSizeQueue<bool>(BUFFER_LENGTH)},
            {GRAB_LEFT_GESTURE, new FixedSizeQueue<bool>(BUFFER_LENGTH)},
            {OPEN_HAND_INWARD_GESTURE, new FixedSizeQueue<bool>(BUFFER_LENGTH)},
            {OPEN_HAND_OUTWARD_GESTURE, new FixedSizeQueue<bool>(BUFFER_LENGTH)}
        };

        private bool _grabbing = false;
        private bool _dragging = false;
        private BodyFrameReader _bodyReader;
        private IList<Body> _bodies;
        private int _frameCount;
        private DateTime _beginFrameCount;
        private  bool _grabbingLeft = false;

        public KeyboardMouseGestures(KinectSensor kinectSensor)
        {
            if (kinectSensor == null)
            {
                throw new ArgumentNullException("kinectSensor");
            }


            // create the vgb source. The associated body tracking ID will be set when a valid body frame arrives from the sensor.
            this.vgbFrameSource = new VisualGestureBuilderFrameSource(kinectSensor, 0);

            // open the reader for the vgb frames
            this.vgbFrameReader = this.vgbFrameSource.OpenReader();
            if (this.vgbFrameReader != null)
            {
                this.vgbFrameReader.IsPaused = false;
                this.vgbFrameReader.FrameArrived += this.Reader_GestureFrameArrived;
            }

            // Load the gestures from the gesture database
            foreach (var db in _dbCollection)
            {
                using (VisualGestureBuilderDatabase database = new VisualGestureBuilderDatabase(db))
                {
                    this.vgbFrameSource.AddGestures(database.AvailableGestures);
                }
            }

            _bodyReader = kinectSensor.BodyFrameSource.OpenReader();
            _bodyReader.FrameArrived += _bodyReader_FrameArrived;

            _frameCount = 0;
            _beginFrameCount = DateTime.Now;

        }

        void _bodyReader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            using (var frame = e.FrameReference.AcquireFrame())
            {
                if (frame != null && frame.BodyCount > 0)
                {
                    if (_bodies == null || _bodies.Count != frame.BodyCount)
                    {
                        _bodies = new Body[frame.BodyCount];
                    }

                    frame.GetAndRefreshBodyData(_bodies);

                    bool grab = false;
                    bool release = false;
                    bool grabLeft = false;

                    ///TODO need to track a specific person
                    // track the first person
                    foreach (var body in _bodies)
                    {
                        if (body.IsTracked)
                        {
                            grab = body.HandRightState == HandState.Closed;
                            release = body.HandRightState == HandState.Open;
                            grabLeft = body.HandLeftState == HandState.Closed;
                            break;
                        }

                    }

                    _grabBuffer.Enqueue(grab);
                    _releaseBuffer.Enqueue(release);

                    _bufferDict[GRAB_LEFT_GESTURE].Enqueue(grabLeft);

                }

                else
                {
                    _grabBuffer.Enqueue(false);
                    _releaseBuffer.Enqueue(true);
                    _bufferDict[GRAB_LEFT_GESTURE].Enqueue(false);
                }
            }

            var positiveCount = _grabBuffer.Where(x => x).Count();
            var releaseCount = _releaseBuffer.Where(x => x).Count();
            if (positiveCount < _grabBuffer.Count * FRAME_ACCURACY && releaseCount >= _releaseBuffer.Count * FRAME_ACCURACY)
            {
                if (_dragging)
                {
                    EndDrag(this, null);
                    _dragging = false;
                    _grabbing = false;
                }
                else if (_grabbing)
                {
                    Clenched(this, null);
                    _dragging = false;
                    _grabbing = false;
                }
            }

            positiveCount = _bufferDict[GRAB_LEFT_GESTURE].Where(x => x).Count();
            if (_bufferDict[GRAB_LEFT_GESTURE].Count == BUFFER_LENGTH)
            {
                if (positiveCount >= _bufferDict[GRAB_LEFT_GESTURE].Count * GRAB_LEFT_FRAME_ACCURACY && !_grabbingLeft)
                {
                    _grabbingLeft = true;
                }
                else if (positiveCount < _bufferDict[GRAB_LEFT_GESTURE].Count * GRAB_LEFT_FRAME_ACCURACY && _grabbingLeft)
                {
                    _grabbingLeft = false;
                    LeftClenched(this, null);
                }
            }
        }

        /// <summary>
        /// Gets or sets the body tracking ID associated with the current detector
        /// The tracking ID can change whenever a body comes in/out of scope
        /// </summary>
        public ulong TrackingId
        {
            get
            {
                return this.vgbFrameSource.TrackingId;
            }

            set
            {
                if (this.vgbFrameSource.TrackingId != value)
                {
                    this.vgbFrameSource.TrackingId = value;
                }
            }
        }

        /// <summary>
        /// Handles gesture detection results arriving from the sensor for the associated body tracking Id
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_GestureFrameArrived(object sender, VisualGestureBuilderFrameArrivedEventArgs e)
        {
            _frameCount++;
            VisualGestureBuilderFrameReference frameReference = e.FrameReference;
            using (VisualGestureBuilderFrame frame = frameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    // get the discrete gesture results which arrived with the latest frame
                    IReadOnlyDictionary<Gesture, DiscreteGestureResult> discreteResults = frame.DiscreteGestureResults;

                    if (discreteResults != null)
                    {
                        foreach (KeyValuePair<Gesture, DiscreteGestureResult> gesturePair in discreteResults)
                        {
                            if (gesturePair.Key.Name == GRAB_RIGHT_GESTURE)
                            {
                                ClickOrDrag(gesturePair.Value);
                                continue;
                            }
                            if (gesturePair.Key.Name == GRAB_LEFT_GESTURE)
                            {
                                continue;
                            }

                            float threshold = DEFAULT_GESTURE_THRESHOLD;
                            _gestureThreshold.TryGetValue(gesturePair.Key.Name, out threshold);
                            FixedSizeQueue<bool> queue;
                            bool positive = false;
                            if (_bufferDict.TryGetValue(gesturePair.Key.Name, out queue))
                            {
                                queue.Enqueue(gesturePair.Value.Detected && gesturePair.Value.Confidence >= threshold);
                                positive = (queue.Count == BUFFER_LENGTH) && (queue.Count * FRAME_ACCURACY < queue.Where(x => x).Count());
                                if (positive)
                                {
                                    queue.Clear();
                                }
                            }
                            if (positive)
                            {
                                switch (gesturePair.Key.Name)
                                {
                                    //case GRAB_LEFT_GESTURE:
                                    //    LeftClenched(this, null);
                                    //    break;
                                    case OPEN_HAND_INWARD_GESTURE:
                                        OpenHandInward(this, null);
                                        break;
                                    case OPEN_HAND_OUTWARD_GESTURE:
                                        OpenHandOutward(this, null);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ClickOrDrag(DiscreteGestureResult result)
        {

            var rightGrab = _bufferDict[GRAB_RIGHT_GESTURE];

            bool positive = result.Detected && (result.Confidence > _confidenceThreshold);
            rightGrab.Enqueue(positive);

            var positiveCount = rightGrab.Where(x => x).Count();
            if (positiveCount >= rightGrab.Count * FRAME_ACCURACY)
            {

                if (!_grabbing)
                {
                    // First frame
                    BeginDrag(this, null);
                    _startGrab = DateTime.Now;
                    _grabbing = true;
                }
                else if (!_dragging && DateTime.Now - _startGrab >= _clenchWindow)
                {
                    _dragging = true;
                    BeginDrag(this, null);
                }
            }


        }

        /// <summary>
        /// Disposes all unmanaged resources for the class
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the VisualGestureBuilderFrameSource and VisualGestureBuilderFrameReader objects
        /// </summary>
        /// <param name="disposing">True if Dispose was called directly, false if the GC handles the disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.vgbFrameReader != null)
                {
                    this.vgbFrameReader.FrameArrived -= this.Reader_GestureFrameArrived;
                    this.vgbFrameReader.Dispose();
                    this.vgbFrameReader = null;
                }

                if (this.vgbFrameSource != null)
                {
                    this.vgbFrameSource.Dispose();
                    this.vgbFrameSource = null;
                }
            }
        }

        /// <summary>
        /// Pause the frame reader
        /// </summary>
        public void Pause()
        {
            this.vgbFrameReader.IsPaused = true;
        }
    }
}
